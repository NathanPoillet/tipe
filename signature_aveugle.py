#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 11:02:11 2021

@author: nathanpoillet
"""
import random
import RSA
from MD5 import MD5


#Prends en argument la clé de signature (e,N) le message m

def encrypt_à_signer(e,N,m,k):
    if RSA.pgcd(k,N)==1:
        return (m * pow(k,e,N))%N
    else:
        print("k n'est pas premier avec N")
        
        
def signe(d,N,m_encrypt):
    return pow(m_encrypt,d,N)


def premiers_à_n(n):
    k=random.randint(2,n//100)
    while RSA.pgcd(k,n)!=1:
        k=random.randint(2,n//100)
    return k






         ###   SIGNATURE PAR BLOCS





## Renvoie une liste correspond au texte transformé en chiffre scindé en plusieurs blocs dont la longueur de chaque blocs
## est le nombre de chiffre qu'il y a dans n
    
def message_en_matrice(text,n):
    
    # l correspond au nombre de chiffre de n
    l=len(str(n))
    nbre_par_bloc=l-1
    char=""
    for k in text:
        char+=str(ord(k))
    nbre_chiffre=len(char)

    ## On fait en sorte qu'il y est autant de chiffre dans chaque
    ##blocs en remplissant par des 0 à la fin du mot
    chiffre_en_plus= nbre_par_bloc -  ((nbre_chiffre)%nbre_par_bloc)
    for k in range(chiffre_en_plus):
        char=char+"0"
        
    ## on forme les l blocs que l'on stocke dans liste 
    u=1
    liste=[char[0]]

    while u!=len(char):
        if u%nbre_par_bloc==0:
            liste.append(char[u])
            u+=1
        else:
            liste[u//nbre_par_bloc]+=char[u]
            u+=1


#    # on transforme la liste en int-liste
    for k in range(len(liste)):
        liste[k]=int(liste[k])
    return liste




#////




def à_signer_ch(e,N,m,k):
    res=[]
    for i in m:
        res.append((i * pow(k,e,N))%N)
    return res
    

def signe_ch(d,N,m_encrypt):
    res=[]
    for i in m_encrypt:
        res.append(pow(i,d,N))
    return res
      





## On suppose que l'utilisateur a par la suite appliqué 
## l'inverse modulaire de k sur l'ensemble de la signature
## de la manière suivante :
    
def signature_ch(sign,k,n):
    k1=RSA.inverse_modulaire(k,n)
    for i in range(len(sign)):
        sign[i]=(sign[i]*k1)%n
    return sign


## retrouve_mess fonctionne d'une manière très semblable
## au déchiffrement de RSA par blocs
    

def retrouve_mess(sign,e,n):
    char=""
    aux1=[]
    for i in range(len(sign)):
        aux1.append(str(pow(sign[i],e,n)) )
        while len(aux1[i]) < len(str(n))-1:
            aux1[i]="0"+aux1[i]
        char=char+aux1[i]
    aux=""
    res=""
    for j in char:
        aux+=j
        if 47<int(aux)<123:
            res+=chr(int(aux))
            aux=""
    return res



def signature_final(e,N,r,k,d):
    mess=MD5(r)
    mat=message_en_matrice(mess,N)
    aux=0
    res=[]
    res2=[]
    for i in mat:
        aux=(i * pow(k,e,N))%N
        res.append(pow(aux,d,N))
        
    k1=RSA.inverse_modulaire(k,N)
    for j in res:
        res2.append( (j*k1) %N)
    return res2



e,n,d=RSA.création_clé()
k=premiers_à_n(n)

m=message_en_matrice(MD5("autre"),n)
t2=signature_final(e,n,"autre",k,d)

r2=retrouve_mess(t2,e,n)
v=r2==MD5("autre")

sign=signe_ch(d,n,à_signer_ch(e,n,m,k))
print(v)




### PB TROUVÉ : Lorsqu'il y a des zéros dans le message ça les fait
### disparaitre


### EXPLICATION
 
 


##s=(sign*k-1)%n = m^d% n donc il suffit que l'utilisateur 

## mette s dans son bulletin comme signature pour que l'on puisse
## vérifier en utilisant la clé publique e que la signature est
## authentique.
## la réponse pourrait 

## les clés (e,d,N) ne sont pas les mêmes qui sont utilisés pour
## chiffré les bulletins pour éviter tout problème