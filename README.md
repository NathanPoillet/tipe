# TIPE



## Introduction

This project was made in 2020-2021 during my Classe Préparatoire aux Grandes Écoles in Lycée Louis Thuilier, Amiens, France.

This was part of my final examination to enter in an engineering school. I've decided to make a whole voting system as we were during the French presidential election, and for learning more about cryptography.

One can vote in secret thanks to encrypted vote with RSA. Everyone got two codes : one for logging in and one that you have to put in the ballot so that we can check later if the elector was authorized to participate. As we only kept the hash of these second keys, nobody has the possibility to use their keys to vote with others code.

The ballot is then signed by and the user can know if the ballot has been changed or not using again RSA and blind signature.

When we checked all the ballot, we can check if the user was authorized to vote ( as the ballot is composed of the answer, a key and the signature ) and the user will be able to find they ballot and find out if it was changed or not.




