#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 19:51:31 2021

@author: nathanpoillet
"""
import random
import string
import RSA
import signature_aveugle
from MD5 import MD5
import XOR


##       GESTION CLE SECRETE

# creation des clés


# partage de la clé avec Shamir en l parties (n à préciser)
#CHANGEMENT : problème avec Shamir -> plus complexe qu'il n'y parait
# utilisation du XOR

def partage_de_clé(nbre_admin):
    e,n,d=RSA.création_clé()
    clé =(e,n)
    liste_clé=XOR.Partage(d,nbre_admin)
    return clé,liste_clé



nbre_admin=int(input("nombre de personne de confiance avec qui partager la clé: "))
clé,liste_clé =partage_de_clé(nbre_admin)
e,n=clé[0],clé[1]
            ##     GESTION DES PARTICIPANTS


#import xlrd
#
#
#msg=input("nom du fichier :")
#
#         
#
#wb = xlrd.open_workbook(msg)
#for s in wb.sheets():
#    table=[] # Creation d'une liste de lignes
#    for ligne in range(s.nrows):
#        table.append([]) # Creation d'une liste par ligne pour les colonnes : table[ligne][colonne]
#        for col in range(s.ncols):
#            table[ligne].append(s.cell(ligne,col).value)
#
## table contient le tableau du fichier xlsx
## la première colone contient "nom prénom", la seconde l'adresse mail,

table=[]




        ##  SIGNATURE 
# on envoie deux codes N1 et N2 aléatoirement aux votants (par mail ?) de taille 12, on garde
# une liste des N1 et une listes des empruntes (par MD5) des N2
# N1 et N2 serviront lors du vote

##  MOT ALEATOIRE: 

def mot_aléatoire():
    caractère= string.ascii_letters + string.digits
    m=''
    for k in range(12):
        m= m+ caractère[random.randint(0,len(caractère)-1)]
    return m
    



nb_participant=len(table)
N1=[]
N2_participant=[]
for i in range(nb_participant):         
    N1.append(mot_aléatoire())
    N2_participant.append(mot_aléatoire())


N2=[]
for k in range(nb_participant):
    N2.append(MD5(N2_participant[k]))

##        GESTION DES QUESTIONS



# input la question :
question = input("la question : ")








##         GESTION DES UTILISATEURS 

vote=[]

## IDENTIFICATIOn : 

#on demande : N1
# si appartient à la liste, on continue
# erreur sinon
#

N1_envoyé=input("N1 :")
if N1_envoyé not in N1:
    print("Erreur le N1 est incorrecte")

##   REPONSE QUESTION

## une fois identifié, la question est posé: input "oui" ou "non"
# vérif si la réponse est "oui"  ou "non"  sinon le vote est nul
# on demande N2. (il sera utilisé au dépouillement)
#singature à l'aveugle à l'aide d'une autre clé RSA de l'admin: on obtiens couple (réponse chiffré,N2,signature de la note)

#On append dans liste des réponses.
#On append N1 dans la liste des "A voté"
    





print(question)
r=input("oui ou non ? ")
 
N2_envoyé=input("N2 :")
bulletin=(r,N2)


#bulletin : ("Réponse : 1"; N2 :rfghjklefd156; signature)

#Les clés de l'administrateur pour la signature aveugle: 


(e_aveu,n_aveu,d_aveu) = RSA.création_clé()


        ## gestion de la signature (votant)

## on ajoute des caractères aléatoires à la réponse puis on applique MD5
r2=2

k=input("k premiers avec n pour la signature aveugle :" )



enc=signature_aveugle.à_signé(e_aveu,n_aveu,r,k)

signature_partiel=signature_aveugle.signe(d_aveu,n_aveu,enc)

## l'utilisateur recoit la signature et rentre son bulletin final :
signature=(signature_partiel * (RSA.inverse_modulaire(k,n_aveu)) )%n_aveu

bulletin=(r,N2,signature)

bulletin=(RSA.RSA(r,e,n),N2,signature)

vote.append(bulletin)
bulletin=()



##    DEPOUILLEMENT

#utilisation des différents parties pour avoir clé secrète

liste_clé_scindée=[]
for k in range(nbre_admin):
    liste_clé_scindée.append(input("partie de clé:"))
    
clé_privée_élection=XOR.retrouve(liste_clé_scindée)

# on déchiffre le bulletin : on vérifie authenticité de la signature par la clé de l'admin
# en resignant sur vote chiffré pour voir si on botient même chose.
# SI oui, on continue
#Sinon vote non pris en compte
# On calcul avec MD5 la N2 pour voir si appartient bien à la liste.
# SI oui, on append dans liste des votes décomptés le couple (réponse,N2)

# Une fois tout les bulletins fait, on compte les réponses, et on déclare le vainqueur



def depouillement(vote,listeN2,clé):
    e,n,d=clé
    resultat=[]
    for bulletin in vote:
        v,N2,signature=bulletin
        if MD5(N2) in listeN2:
            if ((signature*e_aveu)%n_aveu) ==v:
                resultat.append(v)
    return resultat
          
## Dépouillement à améliorer -> pour signature, il vaudrait mieux 
## chiffrer le vote avant de le signer (avec MD5) pour que
## l'admin ne soit vraiment pas capable de regarder la réponse
## donc il faut améliorer RSA pour les caractères

def résultat_final(resultat):
    pour=0
    contre=0
    aucun=0
    for v in resultat:
        if v==oui:
             pour+=1
        elif v==non:
            contre+=1
        else:
            aucun+=1
    return (pour,contre,aucun)
    




## DURANT L'ELECTION
# la liste des bulletins est publique et non mutable par quiconque 
# une fois l'élection finie (temps imparti ou décision des administrateurs aka moi)
# tout est freeze (impossible de voter ou de faire quoi que ce soit pour les admin)
# et le dépouillement se fait, la liste nous déchiffrer reste accessible même après
# l'élection pour pouvoir s'assurer de la véracité des votes.
# Le clé secrète est rendu public pour que chacun puisse s'assurer du déchiffrement



