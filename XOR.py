#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 16:09:59 2021

@author: nathanpoillet
"""

#renvoie le nombre d en binaire
def NombreEnBinaire(d):
    return bin(d)[2:]


# Est la fonction réciproque de la précédente
def BinaireEnNombre(b):
    return int('0b'+b,2)




#7558582039049614201319064441573521
##XOR :


## Ajoute à la chaîne de caractère a, n zéro au début.

def remplissage(a,n):
    for k in range(n):
        a="0"+a
    return a
       
     
  

def xor(a,b):
    res=""
    
    #Remplie si nécessaire pour que a et b aient la même longueur
    
    if len(a)<len(b):
        a=remplissage(a,len(b)-len(a))
    elif len(b)<len(a):
        b=remplissage(b,len(a)-len(b))
        
        
    # Applique un xor sur a et b que l'on stocke dans res
    
    for k in range(max(len(a),len(b))):
        if a[k]==b[k]:
            res=res+'0'
        else:
            res=res+'1'
    return res



    

#////////////
    

from random import randrange


#Renvoie une chaîne de caractère de bits de longueur l

def bin_aléatoire(l):
    a=""
    for k in range(l):
        a=a+str(randrange(0,2))
    return a
        
        
       
#//////////
    
## partage en n clé



## Prends en argument la clé secrète d et le nombre n de personne à qui l'on va partager un "bout" de la clé secrète
## On crée n-1 chaîne de caractère aléatoire de même longueur que la clé d écrit en binaire.
## On applique successivement un xor entre chaque chaîne de caractère crée puis finalement avec la clé d.
## on ajoute le resultat obtenue à la liste que l'on retourne et qui constitue les différentes clés pour retrouver la clé d
    

def Partage(d,n):
    l=[]
    d=NombreEnBinaire(d)
    long=len(d)
    x=""
    for k in range(n-1):
        l.append(bin_aléatoire(long))
    for i in range(n-1):
        x=xor(x,l[i])
    x=xor(d,x)
    l.append(x)
    return l




## Prends en argument une liste de chaîne de caractère de "0" et de "1" et applique xor successivement 
## entre les éléments de l pour retrouver la clé secrète d utilisé lors du Partage
    
def retrouve(l):
    x=''
    for k in range(len(l)):
        x=xor(x,l[k])
    return BinaireEnNombre(x)