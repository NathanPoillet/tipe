#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 12:07:08 2021

@author: nathanpoillet
"""


import RSA
import XOR
def partage_de_clé(nbre_admin):
    (p,q)=RSA.gen_clé()
    n=p*q
    e=RSA.clé_e(p,q)
    d=RSA.clé_d(p,q,e)
    clé =(e,n)
    liste_clé=XOR.Partage(d,nbre_admin)
    return clé,liste_clé






## on crée un clé RSA qui va servir au dépouillement 
# on partage la clé secrète d en un nombre de parties
# que l'on définit