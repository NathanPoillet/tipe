# Créé par poillet, le 02/02/2021 en Python 3.7

from random import randrange



##au+bv=1 algo d'euclide pour obtenir au=1 mod(b)

def Euclide(a,b):
    if b==0:
        return (a,1,0)
    else:
        (pgcd,u,v)=Euclide(b,a%b)
        return (pgcd,v,u-(a//b)*v)
        

def inverse_modulaire(a, m):
    return (Euclide(a,m)[1] % m)



def pgcd(a, b):
    if a < b:
        return pgcd(b, a)
    elif a % b == 0:
        return b;
    else:
        return pgcd(b, a % b)


## calcul du témoin de miller
def temoinMiller(a, n, s, d):
    x = pow(a, d, n)
    if x == 1 or x == n-1:
        return False
    while s > 1:
        x = pow(x, 2, n)
        if x == n-1:
            return False
        s -= 1
    return True

## retourne vrai si n est très probablement premier
def millerRabin(n, t = 50):
    s = 0
    while (n-1)/2**(s+1) == int((n-1)/2**(s+1)):
        s += 1
    d = int(n/2**s)
    for i in range(t):
        a = randrange(2, n-1)
        if temoinMiller(a, n, s, d):
            return False
    return True


#génère deux nombres premier p et q pour le chiffrement RSA
def gen_clé():
    premiers = []
    while len(premiers) != 2:
        a = randrange(10**9, 100**9)
        if a%2 != 0 and millerRabin(a, 25):
            premiers.append(a)

    return premiers[0], premiers[1]




####
def clé_e(p,q):
    e=10
    l=[]
    while len(l)<100:
        if pgcd(e,(p-1)*(q-1)) ==1:
            l.append(e)
        e+=1
    return l[randrange(0,len(l))]

#
#def clé_e(p,q):
#    e=randrange(10,(p-1)*(q-1))
#    while pgcd(e,(p-1)*(q-1)) !=1:
#        e=randrange(10,(p-1)*(q-1))
#    return e


def clé_d(p,q,e):
    return inverse_modulaire(e,(p-1)*(q-1))



def RSA(m,e,n):
    return pow(m,e,n)
    
def decrypt(m,d,n):
    return pow(m,d,n)


def création_clé():
    p,q=gen_clé()
    n=p*q
    e=clé_e(p,q)
    d=clé_d(p,q,e)
    return (e,n,d)

   
    


## RSA SUR DES chaines de caractères
def RSA_ch(text,e,n):
    l=0
    for k in str(n):
        l+=1
    nbre_par_bloc=l-1
    char=""
    for k in text:
        char+=str(ord(k))
    nbre_chiffre=len(char)
    
    ## On fait en sorte qu'il y est autant de chiffre dans chaque
    ##blocs
    
    chiffre_en_plus=nbre_par_bloc - ((nbre_chiffre)%nbre_par_bloc)
    for k in range(chiffre_en_plus):
        char="0"+char
        
    ## on forme les blocs
    u=1
    liste=[char[0]]

    while u!=len(char):
        if u%nbre_par_bloc==0:
            liste.append(char[u])
            u+=1
        else:
            liste[u//nbre_par_bloc]+=char[u]
            u+=1
    
    ## on transforme la liste en int-liste
    
    for k in range(len(liste)):
        liste[k]=int(liste[k])

    
    ## on applique RSA sur chaque blocs avec la clé (e,n)
    
    for k in range(len(liste)):
        liste[k]=RSA(liste[k],e,n)
    return liste
        



def decrypt_ch(liste,d,n):
    char=""
    for k in range(len(liste)):
        liste[k]=decrypt(liste[k],d,n)
        char=char+str(liste[k])
    
    ## MD5 ne semble donner que des chiffres ou des lettres
    ## donc les caractères ont comme code ascii associé un nombre
    ## entre 48 et 122 (-> pas d'accent ni d'espace dans les essai)
    
    res=""
    aux=""
    for k in char:
        aux+=k
        if 47<int(aux)<123:
            res+=chr(int(aux))
            aux=""
    return res
            

        
        

        
    
        
        