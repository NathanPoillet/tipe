﻿##EXEMPLE LITTLE ENDIAN : 0x68 0x75 0x66 0x66 0x6d 0x61 0x6e
#"af" ->b'fa' en little endian

#fonction utile



#fonctions suivantes sur 32 bits

##  int('0xd5',16)






bin(ord('k'))[2:] #transforme k en binaire sur un octet












#//////



## Prends en argument un entier (écris en bits) renvoie
## l'écriture en little endian en 32 bits

def littleEndian32(b):
    bit=b[2:]
    res=""
    for k in range(len(bit)):
        res=bit[k*32:(k+1)*32] +res
    return "0b"+res


# Exemple : In [65]: littleEndian32('0b00000000001010111101110001010100\
##                                     01011101011010110100101110000111')
    

## Out[65]: '0b01011101011010110100101110000111\
##             00000000001010111101110001010100'







## Prends en argument un entier et renvoie son écriture en 64 bits
def binaire64(a):
    b= bin(a & 0xffffffffffffffff)[2:]
    while len(b)<64:
        b="0"+b
    return "0b"+b




## Prends en argument un mot et renvoie le mot en bits avec 
## chaque lettre codé en un octet

def binaireMot8(w):
    message=w.encode("utf-8")
    b=bin(int.from_bytes(message,"big"))[2:]
    while not (len(b)%8==0):
        b="0"+b
    return "0b"+b




## Analogue à littleEndian32 mais pour des octets
    
def littleEndian8(b):
    bit=b[2:]
    res=""
    for k in range(len(bit)):
        res=bit[k*8:(k+1)*8] +res
    return "0b"+res




## Prends un nombre en argument et renvoie une chaîne de caractère 
## correspondant à l'écriture en little endian du nombre en héxadécimal.
    
def littlehex(h):
    a=(hex(h))[2:]
    res=""
    for k in range(len(a),-1,-2):
        res =  res +a[k:]
        a=a[:k]
    return res





## Prends en argument un entier écris en base 2 ayant un nombre de bits multiple à 512
## et crée une liste des bits par bloc de 512 bits.
    
def blocsDe512bits(b):
    res=[]
    a=0
    bit=b[2:]
    for k in range(0,len(bit)):
        if (k+a) > len(bit)-1:
            for i in range(len(res)):
                res[i]="0b"+res[i]
            return res
        else:
            res.append(bit[k+a])
        while (len(res[k]) % 512!=0) :
            res[k]= res[k]+ bit[k+a+1]
            a+=1
    for i in range(len(res)):
        res[i]="0b"+res[i]
    return res






##  PRÉPARATION DU MESSAGE



## Prends un mot en argument, le transforme en bits et le remplie en rajoutant de "0"
## à la fin afin d'obtenir un nombre de bits congru à 448 modulo 512.
## On rajoute ensuite à la fin la longueur du mot en 64 bits en little endian par blocs de 32 bits

def rem(w):
    b=((binaireMot8(w)))[2:]
    l=littleEndian32(binaire64(len(b)))
    b=b+ "1"
    while len(b)%512!=448:
        b+="0"
    b+= l[2:]
    return "0b"+b


##/////////////////////////
    
## On découpe une chaîne de caractère de 512 bits en une liste de 16 mots de 32 bits

def decoupage512en16Motsde32Bits(b):
    res=[]
    bit=b[2:]
    for k in range(16):
        res+=bit[k*32]
        for i in range(1,32):
            res[k]+= bit[k*32+i]
    for j in range(len(res)):
        res[j]="0b"+res[j]
    return res


from math import sin









## On effectue une rotation à gauche de n au nombre x 

def lRotate(x,n):
    x &= 2**32-1
    return ((x<<n) | (x>>(32-n)))


#////
    




##    Algorithme MD5 suivant le pseudo code de wikipedia






def MD5(w):
    
    
    ## INITIALISATION DES VARIABLES :
    
    
    m=blocsDe512bits(rem(w))  #on construit une liste de blocs de 512 bits d'une phrase qui a subit un remplissage
    K= [int(abs(sin(i + 1)) * (2 ** 32)) for i in range(64)]
    r=[7, 12, 17,22,7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,\
    5,9,14,20,5,9,14,20,5,  9, 14, 20,  5,  9, 14, 20,\
    4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,\
    6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21]
    a0=0x67452301
    b0=0xefcdab89
    c0=0x98badcfe
    d0=0x10325476
    f=""
    g=0
    l=[]
    
    
    ## DÉBUT DE LA BOUCLE
    

    for k in range(len(m)):
        b16=decoupage512en16Motsde32Bits(m[k])     
        for k in range(len(b16)-2):
            b16[k]=littleEndian8(b16[k])
        a,b,c,d=a0,b0,c0,d0
        for i in range(64):
            if 0 <= i <=15 :
                f= ((b&c) | ((~b) &d) )
                g=i

            elif 16 <= i <=31 :
                f = (d&b) | (~d &c)
                g= (5*i +1) % 16

            elif 32 <= i <=47 :
                f=b^c^d
                g= (3*i+5) % 16

            elif 48<= i <= 64:
                f=  c^ (b|(~d))
                g=(7*i)%16

            rotate= a+f+K[i] + int(b16[g],2)
                ##print((int(b16[g],2))) ##<--------------------------------
            a=d
            d=c
            c=b
            b= (b +lRotate(rotate,r[i])) % 2**32
            l=l+[(a,b,c,d)]
        a0= ((a0 +a)%2**32)
        b0=((b0 +b)%2**32)
        c0=((c0 +c)%2**32)
        d0=((d0 +d)%2**32)
    return (littlehex(a0)+littlehex(b0)+littlehex(c0)+littlehex(d0))








## Ne semble fonctionner que pour un message d'au maximum 512 bits, l'erreur n'ayant pas été trouvé.




























