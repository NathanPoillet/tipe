#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 13 19:48:15 2021

@author: nathanpoillet
"""
import random
import string
import RSA
from signature_aveugle import *
from MD5 import MD5
import XOR


## OUTILS AU VOTE



def partage_de_clé(nbre_admin):
    e,n,d=RSA.création_clé()
    clé =(e,n)
    liste_clé=XOR.Partage(d,nbre_admin)
    return clé,liste_clé


def mot_aléatoire():
    caractère= string.ascii_letters + string.digits
    m=''
    for k in range(12):
        m= m+ caractère[random.randint(0,len(caractère)-1)]
    return m
    


def créeN1_N2(n):
    nb_participant=n
    N1=[]
    N2_participant=[]
    for i in range(nb_participant):         
        N1.append(mot_aléatoire())
        N2_participant.append(mot_aléatoire())
    N2=[]
    for k in range(nb_participant):
            N2.append(MD5(N2_participant[k]))
    return N1,N2,N2_participant



def k_aveugle(n):
    return premiers_à_n(n)





def test2(e,N,r,k,d):
    mess=MD5(r)
    mat=message_en_matrice(mess,N)
    aux=0
    res=[]
    res2=[]
    for i in mat:
        aux=(i * pow(k,e,N))%N
        res.append(pow(aux,d,N))
        
    k1=RSA.inverse_modulaire(k,N)
    for j in res:
        res2.append( (j*k1) %N)
    return res2




def recomposition_clé_privée(liste):
    return XOR.retrouve(liste)


## 

def depouillement(vote,listeN2,clé_vote,e_aveu,n_aveu):
    e,n,d=clé_vote
    resultat=[]
    for bulletin in vote:
        v,N2,signature=bulletin
        v=RSA.decrypt_ch(v,d,n)
        if MD5(N2) in listeN2:
            if  retrouve_mess(signature,e_aveu,n_aveu)==MD5(v):
                resultat.append(v)
    return resultat

          
def résultat_final_référendum(resultat):
    pour=0
    contre=0
    autre=0
    for v in resultat:
        if v=="oui":
             pour+=1
        elif v=="non":
            contre+=1
        else:
            autre+=1
    return (pour,contre,autre)




## l est la liste des différents candidats
## on range les votes dans l'ordre de la liste
## ie un vote pour le candidat en l[0] est rangé dans res[0]
def résultat_final_élection(resultat,l):
    res=[0]*len(l)
    nul=0
    for k in resultat:
        for i in range(len(l)):
            if k==l[i]:
                res[i]+=1    
    return res






#
#e,n,d=RSA.création_clé()
#k=premiers_à_n(n)
#
#r="40a8712b29ac76182ed0c4f632b7d543"
#m=message_en_matrice(r,n)
#
#t2=test2(e,n,"nul",k,d)


## EXEMPLE D'UN VOTE
    

# 3 parties différentes, 10 participants
# question : "Que pensez vous de faire X ? "


#      INITIALISATION
    
#####
n_votant=10


e_aveu,n_aveu,d_aveu=RSA.création_clé()

(e,n),liste_clé=partage_de_clé(3)

admin1,admin2,admin3=liste_clé[0],liste_clé[1],liste_clé[2]

N1,N2,N2_participant=créeN1_N2(n_votant)

####


#      VOTE

#####



l=["oui","non","autre"]
liste_r=[]
for i in range(n_votant):
    liste_r.append(l[random.randrange(0,3)])

liste_k=[]
for i in range(n_votant):
    liste_k.append(k_aveugle(n_aveu))



liste_signature=[]
for i in range(n_votant):
    sign=test2(e_aveu,n_aveu,liste_r[i],liste_k[i],d_aveu)
    liste_signature.append(sign)


liste_bulletin=[]
for i in range(n_votant):
    bulletin=(RSA.RSA_ch(liste_r[i],e,n),N2_participant[i],liste_signature[i])
    liste_bulletin.append(bulletin)
    
    
#    
#RSA.bulletin = RSA (r / N2 / signature)
#
#dépouillement -> r=decrypt(bulletin) -> decrypt(r[0])
    
    
    
    
    
    
    
    
###    
#######
##    
##    
###      DEPOUILLEMENT
###    
d=recomposition_clé_privée(liste_clé)

resultat=depouillement(liste_bulletin,N2,(e,n,d),e_aveu,n_aveu)




res=résultat_final_référendum(resultat)
print("Pour :",res[0],"contre :",res[1],"autre:",res[2])







####
l1=["Jean","Marion","Manuel","Philip"]
liste_r=[]
for i in range(10):
    liste_r.append(l1[random.randrange(0,4)])

liste_k=[]
for i in range(10):
    liste_k.append(k_aveugle(n_aveu))



liste_signature=[]
for i in range(10):
    sign=test2(e_aveu,n_aveu,liste_r[i],liste_k[i],d_aveu)
    liste_signature.append(sign)


liste_bulletin=[]
for i in range(10):
    bulletin=(RSA.RSA_ch(liste_r[i],e,n),N2_participant[i],liste_signature[i])
    liste_bulletin.append(bulletin)
###    
#######
##    
##    
###      DEPOUILLEMENT
###    
d=recomposition_clé_privée(liste_clé)

resultat=depouillement(liste_bulletin,N2,(e,n,d),e_aveu,n_aveu)

res=résultat_final_élection(resultat,l1)
print()
print(l1)
print(res)



##  A FAIRE


###  REPONSE --> on met dans le bulletin "ouiefefjrfnvhc" -->
###   la réponse est consituée des 3 premiers caractères, les autres
###  servent pour la signature.
N1,N2,N2_participant=créeN1_N2(3)